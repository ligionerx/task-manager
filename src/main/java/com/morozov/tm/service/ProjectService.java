package com.morozov.tm.service;

import com.morozov.tm.entity.Project;
import com.morozov.tm.repository.ProjectRepository;

public class ProjectService {
    private static ProjectRepository projectRepository = new ProjectRepository();

    public static void showAllProject() {
        if (!projectRepository.readAll().isEmpty()) {
            ConsoleHelper.writeString("Список проектов:");
            for (Project p : projectRepository.readAll()) {
                ConsoleHelper.writeString(p.getProjectName());
            }
        } else {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }

    public static void writeProject(String name) {
        if (!name.isEmpty()) projectRepository.write(name);
        else ConsoleHelper.writeString("Имя не введено");
    }

    public static void deleteProject(String name) {
        if (!name.isEmpty()) projectRepository.delete(name);
        else ConsoleHelper.writeString("Имя не введено");
    }

    public static void updateProject(String oldName, String newName) {
        if (!oldName.isEmpty() && !newName.isEmpty()) projectRepository.update(oldName, newName);
        else ConsoleHelper.writeString("Имя не введено");
    }

}
