package com.morozov.tm.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {

    //чтение строки с консоли
    public static String readString() {
        String enterString = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            enterString = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return enterString;
    }

    //вывод строки в консоль
    public static void writeString(String string) {
        System.out.println(string);
    }

    //вывод команд основного меню
    public static void showHelpMenu() {
        writeString("Список команд:");
        writeString("1. Список проектов");
        writeString("2. Список задач");
        writeString("Для выхода введите \"exit\"");
    }

    //вывод воманд для управления проектами
    public static void showProjectCommandList() {
        writeString("Список команд: \n"
                + "1. Добавить новый проект\n"
                + "2. Удалить проект\n"
                + "3. Обновить имя\n"
                + "Для выхода введите \"exit\"");
    }

    //вывод команд для управления задачами
    public static void showTaskCommandList() {
        writeString("Список команд: \n"
                + "1. Добавить новую задачу\n"
                + "2. Удалить задачу\n"
                + "3. Обновить имя задачи\n"
                + "Для выхода введите \"exit\"");
    }
}
